OS_LIST := $(shell ls os)
SUITE_LIST := $(shell cat misc/suites.list)

.PHONY: all
all: test

.PHONY: test
test: $(OS_LIST)

%: os/%
	mkdir -p out
	rm -rf 'out/$*'
	mkdir -p 'out/$*'
	cp -R -t 'out/$*' -- Makefile BSDmakefile GNUmakefile misc $(SUITE_LIST)
	cd 'out/$*' && '../../os/$*'

%-clean: os/%
	rm -rf 'out/$*'

clean:
	rm -rf out
	rm -f os-test.html

.PHONY: html
html: os-test.html

.PHONY: os-test.html
os-test.html: test
	misc/html.sh --enable-legend --enable-suites-overview --os-list "$(OS_LIST)" --suite-list "$(SUITE_LIST)" > os-test.html
