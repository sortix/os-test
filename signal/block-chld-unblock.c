/* Test blocking, raising SIGCHLD, and unblocking. */

#include "signal.h"

static void handler(int signum)
{
	(void) signum;
	printf("SIGCHLD\n");
}

int main(void)
{
	sigset_t sigchld;
	sigemptyset(&sigchld);
	sigaddset(&sigchld, SIGCHLD);
	sigprocmask(SIG_BLOCK, &sigchld, NULL);
	signal(SIGCHLD, handler);
	raise(SIGCHLD);
	sigprocmask(SIG_UNBLOCK, &sigchld, NULL);
	return 0;
}
