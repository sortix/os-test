/* Test raising SIGUSR1. */

#include "signal.h"

static void handler(int signum)
{
	(void) signum;
	printf("SIGUSR1\n");
}

int main(void)
{
	signal(SIGUSR1, handler);
	raise(SIGUSR1);
	return 0;
}
